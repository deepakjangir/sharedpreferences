package cordova.plugin.sharedPreferences;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.content.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class SharedPreferences extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("setSharedPreferences")) {
            // String message = args.getString(0);
            final JSONObject options = (args.length() > 0) ? args.getJSONObject(0) : new JSONObject();
            this.setSharedPreferences(options, callbackContext);
            return true;
        } else if (action.equals("getSharedPreferences")) {
            String message = args.getString(0);
            this.getSharedPreferences(message, callbackContext);
            return true;
        } else if (action.equals("clearValue")) {
            String message = args.getString(0);
            this.clearValue(message, callbackContext);
            return true;
        }
        return false;
    }

    private void setSharedPreferences(JSONObject message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            // SharedPreferences sharedPreferences =
            // PreferenceManager.getDefaultSharedPreferences(context);

            // Editor edit = sharedPreferences.edit();
            // edit.putString("referrer", message);
            // edit.commit();
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void getSharedPreferences(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            Context context = cordova.getActivity().getApplicationContext();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            callbackContext.success(sharedPreferences.getString(message, "Data not found!!"));
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void clearValue(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            Context context = cordova.getActivity().getApplicationContext();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            Editor edit = sharedPreferences.edit();
            edit.remove(message);
            edit.commit();
            callbackContext.success("removed");
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
