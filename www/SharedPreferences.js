var exec = require('cordova/exec');

exports.setSharedPreferences = function (arg0, success, error) {
    exec(success, error, 'SharedPreferences', 'setSharedPreferences', [arg0]);
};
exports.getSharedPreferences = function (arg0, success, error) {
    exec(success, error, 'SharedPreferences', 'getSharedPreferences', [arg0]);
};

exports.clearValue = function (arg0, success, error) {
    exec(success, error, 'SharedPreferences', 'clearValue', [arg0]);
};
